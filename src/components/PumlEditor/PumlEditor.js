import * as ace from 'brace'
import 'brace/mode/text'
import 'brace/theme/monokai'

export default {
  name: 'PumlEditor',
  data () {
    return {
      title: 'PlantUML'
    }
  },
  computed: {
    puml () {
      return this.$store.state.puml
    },
    preview () {
      return this.$store.state.preview
    },
    visibilityIcon () {
      return this.$store.state.preview ? 'visibility_off' : 'visibility'
    },
    href () {
      let blob = new Blob([this.puml], {
        type: 'octet/stream'
      })
      return window.URL.createObjectURL(blob)

    }
  },
  methods: {
    updatePuml: function (puml) {
      this.$store.commit('updatePumlContent', puml)
    },
    downloadPuml: function (event) {
      this.download = 'foo.pml'
    },
    togglePreview: function () {
      this.$store.commit('togglePreview')
    }
  },
  mounted: function () {
    let vm = this
    let editor = ace.edit('puml-editor')
    editor.setOptions({
      mode: 'ace/mode/text',
      theme: 'ace/theme/monokai'
    })
    editor.$blockScrolling = Infinity
    let session = editor.getSession()
    session.setValue(this.puml)
    session.on('change', function () {
      vm.updatePuml(session.getValue())
    })
  }
}
