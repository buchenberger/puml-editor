import plantumlEncoder from 'plantuml-encoder'

export default {
  name: 'PumlViewer',
  data () {
    return {
      title: 'UML'
    }
  },
  computed: {
    url () {
      return `https://www.plantuml.com/plantuml/svg/${plantumlEncoder.encode(this.$store.state.puml)}`
    }
  }
}
