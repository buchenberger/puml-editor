// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'
import App from './App'
import router from './router'

Vue.config.productionTip = false
Vue.use(Vuex)

const store = new Vuex.Store({
  plugins: [
    createPersistedState(
      {
        'key': 'puml_cache',
        'paths': [
          'puml'
        ]
      }
    )],
  state: {
    puml: '',
    preview: false
  },
  mutations: {
    updatePumlContent (state, data) {
      state.puml = data
    },
    togglePreview (state, data) {
      state.preview = !state.preview
    }
  }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
